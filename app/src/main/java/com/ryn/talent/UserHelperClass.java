package com.ryn.talent;

public class UserHelperClass
{
    String country,subscription;

    public UserHelperClass() {
    }

    public UserHelperClass(String country, String subscription)
    {
        this.country = country;
        this.subscription = subscription;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }
}
