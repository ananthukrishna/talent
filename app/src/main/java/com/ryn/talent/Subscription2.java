package com.ryn.talent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

public class Subscription2 extends AppCompatActivity {
    Button sub,logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription2);
        sub = findViewById(R.id.sub);
        logout = findViewById(R.id.logout);

        sub.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(isNetworkConnected()) {
                    Intent intent = new Intent(Subscription2.this, Subscription.class);
                    startActivity(intent);
                }
                else
                {
                    Snackbar.make(findViewById(android.R.id.content), "Check internet connection", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.RED)
                            .show();
                }
            }
        });
        logout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(isNetworkConnected()) {

                    SharedPreferences sPrefs = getSharedPreferences("sharedPreferences", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sPrefs.edit();
                    editor.putLong("timeleft", 0);
                    editor.putLong("sysstoptime", 0);
                    editor.apply();

                    FirebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(Subscription2.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else
                {
                    Snackbar.make(findViewById(android.R.id.content), "Check internet connection", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.RED)
                            .show();
                }
            }
        });

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}