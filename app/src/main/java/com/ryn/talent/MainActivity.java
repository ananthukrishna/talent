package com.ryn.talent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity
{
AutoCompleteTextView autoCompleteTextView;
EditText editText;
TextInputLayout textInputLayout,phone;
String country,code,pn;
Button button;
ArrayList<String> codeArrayList = new ArrayList<String>(Arrays.asList(CountryDetails.getCode()));
ArrayList<String> countryArrayList = new ArrayList<String>(Arrays.asList(CountryDetails.getCountry()));

@Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textInputLayout = findViewById(R.id.autoComplete);
        phone = findViewById(R.id.phonetext);
        phone.requestFocus();
        autoCompleteTextView = (AutoCompleteTextView) textInputLayout.getEditText();
        button = findViewById(R.id.button);
        editText = findViewById(R.id.phone);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, countryArrayList);

        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.setText(countryArrayList.get(94));
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3)
            {
                textInputLayout.setError(null);
            }
        });

        editText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                phone.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()) {
                    if (setupcountry() && setupphone()) {
                        String phonenumber = code + pn.trim();

                        Intent intent = new Intent(MainActivity.this, VerifyPhoneActivity.class);
                        intent.putExtra("phoneNumber", phonenumber);
                        intent.putExtra("country", country);
                        startActivity(intent);
                    }

                }
                else
                {
                    Snackbar.make(findViewById(android.R.id.content), "Check internet connection", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.RED)
                            .show();
                }
            }
        });
    }
    public boolean setupcountry()
    {
        String v = String.valueOf(autoCompleteTextView.getText());
        int x = countryArrayList.indexOf(v);
        if(!v.isEmpty() && x!=-1)
        {
            country = countryArrayList.get(x);
            code = codeArrayList.get(x);
            return true;
        }
        else
        {
            textInputLayout.setError("choose country from the list");
            autoCompleteTextView.setText("");
            return false;
        }
    }
    public  boolean setupphone()
    {
         pn = String.valueOf(editText.getText());
        if(!pn.isEmpty() && pn.length()<16 && pn.length()>6)
            return true;
        else
        {
            phone.setError("valid phone number required");
            return false;
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}