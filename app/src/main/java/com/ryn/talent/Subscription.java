package com.ryn.talent;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import rubikstudio.library.LuckyWheelView;
import rubikstudio.library.model.LuckyItem;

import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;

public class Subscription extends AppCompatActivity {
    ArrayList<String> list = new ArrayList<>();
    FirebaseDatabase rootnode;
    DatabaseReference user,wheel;
    TextView textView;
    LuckyWheelView luckyWheelView;
    UserHelperClass userHelperClass;

    List<LuckyItem> data = new ArrayList<>();
    long timee,timesLeft;
    int YOUR_VALUE_TO_COUNTDOWN = 15;
    CountDownTimer countDownTimer;
    Button spin,sub;
    String phoneNumber,country,s1,s2,s3,s4,s5,s6;
    LuckyItem luckyItem1, luckyItem2, luckyItem3, luckyItem4, luckyItem5, luckyItem6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        textView = findViewById(R.id.textView3);
        spin = findViewById(R.id.play);
        sub = findViewById(R.id.sub);

        SharedPreferences prefs =  getApplicationContext().getSharedPreferences("USER_PREF", Context.MODE_PRIVATE);
        phoneNumber = prefs.getString("phoneNumber", NULL);
        country = prefs.getString("country", NULL);

        userHelperClass = new UserHelperClass(country,"yes");
        rootnode = FirebaseDatabase.getInstance();
        user = rootnode.getReference("users").child(phoneNumber);
        wheel = rootnode.getReference("wheel");

        wheel.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();
                for(DataSnapshot dataSnapshot:snapshot.getChildren())
                {
                    list.add(dataSnapshot.getValue().toString()) ;
                }
                s1 = list.get(0).trim()+"%";
                s2 = list.get(1).trim()+"%";
                s3 = list.get(2).trim()+"%";
                s4 = list.get(3).trim()+"%";
                s5 = list.get(4).trim()+"%";
                s6 = list.get(5).trim()+"%";
                textView.setText("Get a chance to win upto "+s6+" discount on your subscription fee");
                set();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        sub.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new MaterialAlertDialogBuilder(Subscription.this)
                        .setTitle("Subscribe")
                        .setMessage("To subscribe you must pay a fee.")
                        .setCancelable(false)
                        .setPositiveButton("Pay now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                user.setValue(userHelperClass);
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                Toast.makeText(Subscription.this, "Cancelled", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });

        spin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = getRandomIndex();
                luckyWheelView.startLuckyWheelWithTargetIndex(index);
                spin.setEnabled(false);
            }
        });
    }

    private void set()
    {
        luckyWheelView = (LuckyWheelView) findViewById(R.id.luckyWheel);
        luckyItem1 = new LuckyItem();
        luckyItem2 = new LuckyItem();
        luckyItem3 = new LuckyItem();
        luckyItem4 = new LuckyItem();
        luckyItem5 = new LuckyItem();
        luckyItem6 = new LuckyItem();
        data.clear();

        luckyItem1.topText = s1;
        luckyItem1.color = 0xffFFF3E0;
        data.add(luckyItem1);

        luckyItem2.topText = s2;
        luckyItem2.color = 0xffFFE0B2;
        data.add(luckyItem2);

        luckyItem3.topText = s3;
        luckyItem3.color = 0xffFFCC80;
        data.add(luckyItem3);

        luckyItem4.topText = s4;
        luckyItem4.color = 0xffFFF3E0;
        data.add(luckyItem4);

        luckyItem5.topText = s5;
        luckyItem5.color = 0xffFFE0B2;
        data.add(luckyItem5);

        luckyItem6.topText = s6;
        luckyItem6.color = 0xffFFCC80;
        data.add(luckyItem6);


        luckyWheelView.setData(data);
        luckyWheelView.setRound(5);

        luckyWheelView.setLuckyRoundItemSelectedListener(new LuckyWheelView.LuckyRoundItemSelectedListener() {
            @Override
            public void LuckyRoundItemSelected(int index) {
                timee = YOUR_VALUE_TO_COUNTDOWN * 60000;
                startTimer();

                new MaterialAlertDialogBuilder(Subscription.this)
                        .setTitle("Congratulations!")
                        .setMessage("You got a "+ data.get(index).topText + " discount on your subscription fee")
                        .setCancelable(false)
                        .setPositiveButton("Pay now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                //add payment here
                                user.setValue(userHelperClass);
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(Subscription.this, "Cancelled", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });

    }

    private int getRandomIndex() {
        Random rand = new Random();
        return rand.nextInt(data.size() - 1) + 0;
    }

    private int getRandomRound() {
        Random rand = new Random();
        return rand.nextInt(10) + 15;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(timee, 1000) {
            @Override
            public void onTick(long l) {
                long minutes = (l / 1000)  / 60;
                int seconds = (int)((l / 1000) % 60);
                String time = String.valueOf(minutes) + " : "+String.valueOf(seconds);
                spin.setText(time);
                spin.setEnabled(false);
                timesLeft = l;
            }

            @Override
            public void onFinish() {
                spin.setText("SPIN NOW");
                spin.setEnabled(true);
            }
        }.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences sPrefs = getSharedPreferences("sharedPreferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putLong("timeleft", timesLeft);
        editor.putLong("sysstoptime", System.currentTimeMillis());
        editor.apply();
        countDownTimer.cancel();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sPrefs = getSharedPreferences("sharedPreferences", MODE_PRIVATE);
            timesLeft = sPrefs.getLong("timeleft", 0);
            long stopTime = sPrefs.getLong("sysstoptime", 0);
            long currentTime = System.currentTimeMillis();
            timee = timesLeft - (currentTime - stopTime);
                startTimer();
    }
}